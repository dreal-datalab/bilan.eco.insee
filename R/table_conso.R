
#' table_conso : table des indicateurs d'evolution des conso aux echelles regionale et France entiere
#'
#' @param df le df pret pour la visualisation ggplot, produit par `finit_conso()`
#' @param an l'annee observee au format numerique
#'
#' @return Un dataframe des indicateurs principaux a prendre en compte pour le commentaire sur les consommations
#' @export
#' @importFrom dplyr arrange mutate select group_by bind_rows summarise first last filter lag lead across ungroup 
#' @importFrom tidyselect vars_select_helpers
#' @importFrom tricky str_standardize
#' @importFrom rlang .data
#'
#' @examples
#' \dontrun{
#' table_conso(df = conso_2, an = 2020)
#' }
table_conso <- function(df, an = 2020) {
  
  tble_conso <-  df %>% 
    dplyr::arrange(.data$annee, .data$id_zone, .data$variable) %>% 
    dplyr::mutate(entete = paste0(.data$variable, " - ", .data$id_zone), annee = as.character(.data$annee)) %>% 
    dplyr::select(.data$entete, .data$annee, .data$valeur) %>% 
    dplyr::group_by(.data$entete) 
  
  tble_conso <- dplyr::bind_rows(tble_conso, dplyr::summarise(tble_conso, annee = paste0("Moy ", dplyr::first(.data$annee), " - ", dplyr::last(.data$annee)), 
                                                              valeur = mean(.data$valeur), .groups = "keep")) %>% 
    dplyr::filter(.data$annee >= as.character(an - 1)) %>% 
    dplyr::mutate(evol_n_nmoins1 = ((.data$valeur - dplyr::lag(.data$valeur))/dplyr::lag(.data$valeur)*100) %>% round(1),
                  ecart_moy = ((.data$valeur - dplyr::lead(.data$valeur))/dplyr::lead(.data$valeur)*100 )%>% round(1)) %>% 
    dplyr::ungroup()
  
  lib_col <- unique(tble_conso$annee)
  
  tble_conso2 <- dplyr::filter(tble_conso, .data$annee == lib_col[2]) %>% 
    dplyr::select(-.data$annee) %>% 
    dplyr::arrange(.data$entete)
  
  names(tble_conso2)  <- c(
    names(tble_conso2)[1],
    paste(names(tble_conso2)[2], lib_col[2], sep ="_"),
    paste("evol", lib_col[1], lib_col[2], sep = "_"),
    paste0("ecart_", tricky::str_standardize(lib_col[3]))
    )
  
 return(tble_conso2)
  
}


#' Visualisation de la table des indicateurs d'evolution des conso aux echelles regionale et France entiere
#'
#' @param df  le df pret pour la visualisation flextable, produit par `table_conso()`
#'
#' @return  le widget flextable des indicateurs d'evolution des consommations
#' @importFrom flextable flextable set_header_labels set_table_properties bg color
#' @export
#'
#' @examples
#' \dontrun{
#' table_conso_viz(df = tble_conso2)
#' }
table_conso_viz <- function(df) {
  flextable::flextable(df) %>%
    flextable::set_header_labels(entete = "indicateur - territoire") %>%
    flextable::set_table_properties(layout = "autofit") %>%
    flextable::bg(i = 1, bg = "#7D4E5B", part = "header") %>%
    flextable::color(i = 1, color = 'white', part = "header")
}
